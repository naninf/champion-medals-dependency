# Champion Medals dependency example

An example Openplanet plugin demonstrating how
[Champion Medals](https://gitlab.com/naninf/champion-medals) can be used as an
optional dependency. This plugin creates a small window showing the state of
the Champion medal time as provided by the plugin.

Note that Openplanet has no built-in version requirement for dependencies, so
any dependency plugin will only work if the functions it uses are also
available in the installed version of the source Champion Medals plugin.
