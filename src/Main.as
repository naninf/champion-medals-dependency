void Render() {
    int window_flags = UI::WindowFlags::NoResize |
                       UI::WindowFlags::NoScrollbar |
                       UI::WindowFlags::NoScrollWithMouse |
                       UI::WindowFlags::AlwaysAutoResize |
                       UI::WindowFlags::NoDocking;

    UI::Begin("Champion Medals dependency example", window_flags);

    // Check whether Champion Medals is installed, as it is configured to be an
    // optional dependency for this plugin.
#if DEPENDENCY_CHAMPIONMEDALS

    // Get the time for the Champion medal in ms. Returns 0 if no Champion
    // medal is available (either because the player is not in a map, the map
    // has no Champion medal, or the request failed).
    uint cm_time = ChampionMedals::GetCMTime();

    if (cm_time == 0) {
        UI::Text("No Champion medal.");
    } else {
        UI::Text("Champion medal: " + cm_time + " ms.");
    }

#else

    UI::Text("Champion Medals is not installed.");

#endif

    UI::End();
}
